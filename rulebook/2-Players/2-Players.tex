%! TEX root = ../rulebook.tex

\section{Player Creation and Transactions}

\subsection{Roster information}
\begin{deepEnumerate}
	\item Rosters must have a minimum of 9 players including 1 pitcher.	A team may not field a roster of over 20 players.
	\begin{deepEnumerate}
		\item A pinch-hitting GM (see \hyperref[sec:GMs as Players]{GMs as Players}) does not occupy a roster slot.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Player creation}
\begin{deepEnumerate}
	\item Player names submitted by a player cannot be changed without approval from the Office of the Commissioners.
	\begin{deepEnumerate}
		\item Names may not match or strongly resemble those of active MLB players.
		\item Names may not bully or demean other players or members of the community.
		\item Players will have a limit on name changes.
		\begin{deepEnumerate}
			\item Offseason name changes are allowed.
			\item Midseason name changes are limited to one per player.
			\item OOTC reserves the right to make exceptions to this rule.
		\end{deepEnumerate}
		\item No alternate accounts may be used. Players and GMs are limited to using one account in the game.
		Discovery of the use of an alternate account will result in an immediate and permanent ban of the player
		and all current, past, and future accounts used or held by that player.
		\begin{deepEnumerate}
			\item Reporter accounts are the only exception (see \hyperref[sec:reporters]{reporter rules})
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Players are required to have a Reddit account in good standing to sign up as determined by the Moderators.
	\begin{deepEnumerate}
		\item Reddit accounts under 6 months of age and with minimal karma may be rejected as part of our policy on alternate accounts.
		\begin{deepEnumerate}
			\item Accounts not meeting these requirements may be vouched for by existing members of the community in good standing.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item All players will choose to be a hitter or pitcher, a hand, and a type.
	\begin{deepEnumerate}
		\item Hitters pitching in MLR will be assigned the Position pitching type. Pitchers hitting in MLR will be assigned the Pitcher hitting type.
		\item Hitters pitching in MiLR may select a pitching type. Pitchers hitting in MiLR may select a hitting type. These types will only be used in MiLR games.
		\item Position players have the opportunity to choose their primary and secondary positions.
		Available secondary positions for each primary position are listed below.
	
		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				Primary position & Available secondary positions         \\
				\hline
				Catcher          & First base, third base                \\
				\hline
				First base       & Third base, left field                \\
				\hline
				Second base      & Shortstop, first base, right field    \\
				\hline
				Third base       & Second base, shortstop, left field    \\
				\hline
				Shortstop        & Second base, third base, center field \\
				\hline
				Left field       & Right field, first base               \\
				\hline
				Center field     & Left field, right field               \\
				\hline
				Right field      & Center field, first base              \\
				\hline 
			\end{tabular}
		\end{center}
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Lineup creation}
\label{sec:lineups}
\begin{deepEnumerate}
	\item GMs are required to send in their lineups prior to the start of the session. Starting lineups must be valid when submitted.
	Invalid starting lineups must be fixed as soon as possible, or risk being overridden by a default lineup 
	or one set by the League Operation Managers (see \nameref{sec:game rules}). 
	\begin{deepEnumerate}
		\item A valid lineup consists of 9 (or 10 when using a DH) players,	in positions they are each eligible for, with all positions represented.
		\begin{deepEnumerate}
			\item Players are eligible to play their primary, secondary, or third positions.
			\item Players playing out of position may be allowed on a case-by-case basis with valid reason,	and must be approved by the League Operation Managers.
		\end{deepEnumerate}
		\item Pitchers may only play as pitchers. They may not play a defensive position other than pitcher.
		\begin{deepEnumerate}
			\item Pitchers may pinch hit or play a position in case of emergency.
		\end{deepEnumerate}
		\item Any player is eligible to slot in as DH.
		\item Since catcher is not an available secondary position,	a backup "emergency" catcher may be designated prior to each game.
		\item Position player pitching
		\begin{deepEnumerate}
			\item Position players may come in to pitch in a game when both of the following are true:
			\begin{deepEnumerate}
				\item All pitchers on the team's roster are unavailable.
				\item The umpire believes the substitution of a position player to pitch is necessary or justified.
			\end{deepEnumerate}
			\item Position players that pitch will never get a hand bonus and have a special pitching range (see \nameref{sec:ranges index}).
		\end{deepEnumerate}
		\item GMs as players
		\label{sec:GMs as Players}
		\begin{deepEnumerate}
			\item GMs have two options if they would like to play in a game.
			\begin{deepEnumerate}
				\item Full time player-GM
				\begin{deepEnumerate}
					\item Full time player-GMs occupy a roster spot on their team and are eligible to start in a game for their team.
					\item Player GMs choose a hitter or pitcher type, positions (if hitter), and hand. Player GMs can change these during the offseason.
				\end{deepEnumerate}
				\item Pinch hitter GM
				\begin{deepEnumerate}
					\item Pinch hitter GMs do not occupy a roster slot on their team and are not eligible to start in a game for their team.
					\item Pinch hitter GMs choose a hitter type, pitcher type, and hand. Pinch hitter GMs can change these during the offseason.
					\item Pinch hitter GMs may pinch hit for any player at any point in the game. They assume the position of the player they are replacing,
					but cannot be moved to other positions once in the game.
				\end{deepEnumerate}
			\end{deepEnumerate}
		\end{deepEnumerate}
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Signing free agents}
\begin{deepEnumerate}
	\item After signing up, players will be posted to the GM channel and the free agent list by the Moderators. Once posted, they enter a 12-hour no-signing period.
	\begin{deepEnumerate}
		\item Free agents cannot officially accept a contract until the 12-hour window has passed.
		This time should be used to consider offers from every team who extends one.
		\item The time and date of the no-sign period will be included on the free agent listing.
	\end{deepEnumerate}
	\item Free agents signed are not eligible to play for their MLR team during the current MLR session.
	\begin{deepEnumerate}
		\item Free agents gifted by the Office of the Commissioners are exempt from this rule.
		\item Newly signed players are eligible to play in MiLR during the session they are signed.
	\end{deepEnumerate}
	\item To sign a free agent to a team, a GM must post a confirmation from the player	(date included) to the GM channel in the Discord.
	\item The League Operation Managers reserve the right to deny free agent signings if they do not follow procedure or in the case of conflict,
	such as multiple teams claiming a free agent.
	\item Players that are not listed on the free agent list may not be signed prior to their posting.
	This includes free agents who have been wiped from the list	(they will need to sign up and be posted again).
	\item A free agent may inform the League Operation Managers they are retiring, in which case they will be removed from the list.
\end{deepEnumerate}

\subsection{Releasing players/Waivers}
\begin{deepEnumerate}
	\item A player may be released at any time for any reason.
	\item Players will be placed on three-day waivers when released.
	\begin{deepEnumerate}
		\item For the first three sessions of the season, Waiver Claim order is the same as the draft order in the most recent draft.
		\item After Session 3 ends, the Waiver Claim order for each player will be determined by applying the \hyperref[sec:Draft order]{Draft order}
		to the league standings	as of the last session in which all games have been completed.
		\begin{deepEnumerate}
			\item If a session has been completed but games are continuing past the end of the session, 
			that session will be used and the records of those teams once those games have been completed will be used.
		\end{deepEnumerate}
		\item If the player clears waivers, they revert to free agent status and may be signed by any team.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Placing players on hiatus}
\begin{deepEnumerate}
	\item If a player knows they will unavailable for a time, their GM may place them on the hiatus list.
	\begin{deepEnumerate}
		\item Valid hiatus reasons include, but are not limited to,	parental responsibilities, vacations, mental health, school reasons, etc.
		\item The Office of the Commissioners must approve all hiatus list additions,	and GMs must provide proof of a player's situation to corroborate the request.
		\item Teams must be able to field a valid lineup (see \nameref{sec:lineups}) without the player being moved to hiatus.
	\end{deepEnumerate}
	\item Players on the hiatus list do not occupy a roster slot and may not play in any game, but remain under team control.
\end{deepEnumerate}

\subsection{Retired Players}
\begin{deepEnumerate}
	\item A player may retire any time.
	\begin{deepEnumerate}
		\item Retirement immediately removes the player from the team’s active roster.
		\item The retired player must be substituted out of any game they are currently playing.
	\end{deepEnumerate}
	\item If the player returns in the same season in which they retired, the player will be returned to the team they left upon retirement.
	\begin{deepEnumerate}
		\item GMs may return the player to the active roster, release the player, or trade the player. This must be done prior to the start of the
		following session. Failure to take action on a returning player will result in releasing the player.
		\begin{deepEnumerate}
			\item GMs will have at least 48 hours to take action. If the player returns with fewer than 48 hours remaining in the current session,
			the decision will be due by the end of the following session.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item If a retired player returns after the Paper Cup of the season they retired in, they will be declared a free agent.
	\item The rights of retired players who have not returned cannot be traded.
	\item Returning retired players may not change any details about their player (type, hand, position) except to the extent otherwise allowed by the rules.
\end{deepEnumerate}

\subsection{Trading}
\begin{deepEnumerate}
	\item Trades may occur between two or more MLR teams.
	\begin{deepEnumerate}
		\item A trade is finalized when all teams involved in the trade confirm it in the GM channel, subject to OOTC approval.
		\item The players involved in a trade are moved to their new teams after the current session, prior to the start of the next session.
	\end{deepEnumerate}
	\item Good Faith Trading
	\begin{deepEnumerate}
		\item All GMs must participate in trade discussions in good faith.
			\begin{deepEnumerate}
			\item Good Faith is here defined as not grossly misrepresenting the activity of a player either in their level of discord activity or the last time they were active.
			\end{deepEnumerate}
		\item If any GM is found to have misrepresented the conditions of the trade, that trade is void unless renegotiated
		\item Any trade that is found to grossly benefit one team may be overturned at the discretion of the OOTC 
	\end{deepEnumerate}
	\item Trade Deadline
	\begin{deepEnumerate}
		\item The trade deadline is set at 12pm ET on the first day of session 13.
		\item Players to be named later and Future Considerations cannot be settled by moving players after the trade deadline.
		You must either settle these before the deadline or during the offseason.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Replacement of general managers}
\begin{deepEnumerate}
	\item In the event of an unplanned departure of a GM, the team's primary captain becomes acting GM.	Their status as a player is unaffected.
	The League Operation Managers or Moderators can choose the acting GM at their sole discretion if the primary team captain is not suitable for any reason.
	\begin{deepEnumerate}
		\item Departure here is defined as the GM leaving the GM position for any reason, including but not limited to stepping down, retiring, or being removed via mutiny.
		\begin{deepEnumerate}
			\item Discord presence is strongly recommended but not explicitly required.	In the case of a Discord absence a player must be appointed
			to represent the team in the GM channel. They will be deemed an Interim GM.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item The team may collectively put forth a teammate for consideration for the GM position.
	\item In the event no teammate is chosen or in cases of their being denied by the League Operation Managers or Moderators,
	the League Operation Managers or Moderators will submit 3 candidates to the team. One of these candidates must be chosen by the team to become the new GM.
	\item The Mutiny Clause: Additional conditions for GM removal
	\begin{deepEnumerate}
		\item 3 missed lineup submissions
		\item 80\% or 10 players vote of no confidence from team
		\item 2 games lost to Auto-K/BB forfeit or 10 Auto-K/BBs in any 4 game period.
		\item 75\% of all GMs vote no confidence.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Team captains}
\begin{deepEnumerate}
	\item GMs may designate a primary team captain and up to two secondary team captains.
	These players must be disclosed to the League Operation Managers and they can be added or removed at any time.
	\item Team captains can make emergency substitutions and set emergency lineups in the case of an unavailable GM
	(see \hyperref[sec:substitutions]{Substitutions} and \hyperref[sec:captain lineups]{Captain Lineups}).
	\begin{deepEnumerate}
		\item Umps should not initiate contact with team captains except in extreme cases or otherwise stated in the rulebook.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Midseason Positional Changes}
\begin{deepEnumerate}
	\item A pitcher can opt out of pitching at any time during the season prior to the trading deadline.
	\begin{deepEnumerate}
		\item The player's new position will be Designated Hitter for the rest of the season.
		\begin{deepEnumerate}
			\item The player may start at DH.
			\item The player can pinch hit, but they cannot take the field. Pinch Hitting for a fielding player means they must be removed from the game 
			once their team goes back to the field.
			\item Designated Hitter players are unable to modify their position during the season by any method except as a \hyperref[sec:MiLR FA]{Minor League Free Agent}.
			\item Designated Hitter Players are unable to add a \hyperref[sec:Third position]{third position} during the season. They still gain credit towards an offseason change.
		\end{deepEnumerate}
		\item The player will be able to choose any batting type.
		\item After the season, a Designated Hitter may select any valid primary and secondary position combination.
		\begin{deepEnumerate}
			\item A Designated Hitter changing back to pitcher must enter the draft.
		\end{deepEnumerate}
		\item Any opt out requests during a session will be processed before the beginning of the next session.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Offseason Positional Changes}
\label{sec:OPC}
\begin{deepEnumerate}
	\item Each offseason, a player may opt to change their position freely to any other legal position.
	\item A player may change positions freely pursuant to the \hyperref[sec:Third position]{Third position rules}.
	\item A position player wishing to change their position in any way not covered by the \hyperref[sec:Third position]{Third position rules} must enter the draft.
	\item A pitcher changing positions to become a position player is free to remain with their team or depart their team pursuant to the
	\hyperref[sec:OPM]{Offseason Player Movement rules}.
\end{deepEnumerate}

\subsection{Offseason Player Movement}
\label{sec:OPM}
\begin{deepEnumerate}
	\item At the end of each season, players may opt to depart from the Club to which they are currently signed.
	\begin{deepEnumerate}
		\item Players who signed with their club prior to the Trading Deadline, or who have appeared in at least 6 regular season MLR or MiLR games, shall be eligible
		to opt into the free-agent pool. Games played in MiLR as a free agent count towards this minimum.
		\item All players opting to depart from their club who do not opt into the free-agent pool shall be part of the draft pool.
		\item Players changing position under the \hyperref[sec:OPC]{Offseason Positional Changes rules} remain subject to those rules.
	\end{deepEnumerate}
	\item Once the list of players departing clubs at the end of the season has been finalized, General Managers can release players still members of their Club.
	\begin{deepEnumerate}
		\item Players released this way shall be subject to the same rules as players who opted to depart from their club.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{The Draft}
\begin{deepEnumerate}
	\item Number of rounds
	\begin{deepEnumerate}
		\item The Starting number of rounds will be 6
		\item The number of rounds may be changed depending on the amount of sign-ups
	\end{deepEnumerate}
	\item Number of picks
	\begin{deepEnumerate}
		\item Teams may draft up to their roster cap (currently 20 players). Teams may pass at any round.
		If a team passes, they are passing on all remaining rounds. There is no minimum number of players a team must draft.
		\item Teams may trade picks. A team will be able to claim their own or acquired (via trade) picks in that round if their player roster is below the current roster limit.
		A team that has traded their pick for that round must also have a player roster below the current roster limit.
		\begin{deepEnumerate}
			\item  If a team can not provide the promised pick, the standard procedure will be that
			the team given a faulty pick shall select a player from the guilty team's roster. The GM of the guilty
			team's roster may protect a number of players equal to the draft round in question. For example, if
 			team A promised a 3rd round pick and cannot provide it, Team B may select any player from Team A's
 			roster except 3 protected players decided by Team A's GM.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Draft order
	\label{sec:Draft order}
	\begin{deepEnumerate}
		\item The Draft Order shall follow the following rules:
		\begin{deepEnumerate}
			\item Teams that did not qualify for the playoffs, starting with the worst record and ending with the best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league’s Wild Card Series, starting with the worst record and ending with the best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league’s Divisional Series, starting with the worst record and ending with the best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league’s Championship Series, starting with the worst record and ending with the best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item MLR Runner-Up
			\item Paper Cup Champion
		\end{deepEnumerate}
		\item For any ties within any category, the tie shall be broken by strength of schedule, with the lowest percentage getting the highest pick.
		\item For any ties remaining after the strength of schedule tiebreaker, apply all of the following:
		\begin{deepEnumerate}
			\item Determine the lowest seeded team among teams in the same division using the \hyperref[sec:Division tiebreakers]{Division tiebreakers}
			\item Determine the lowest seeded team among remaining teams in the same league using the \hyperref[sec:Wildcard tiebreakers]{Wildcard tiebreakers}
			\item Determine the lowest seeded team among remaining interleague teams using the following:
			\begin{deepEnumerate}
				\item Team losing head-to-head
				\item Lower win percentage against common opponents (minimum of four games)
				\item Lower strength of victory
				\item Worse run differential
				\item Most Autos
				\item Random Number Generator
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item Continue running the tiebreaker from the beginning with the remaining teams until all ties are broken.
		\item For any draft order decided by tiebreaker, the draft order of those teams will be reversed in all even rounds (snaked).
		\item If expansion teams are added, they will receive 2 picks at the start of each round in a random, snaking order.
	\end{deepEnumerate}
	\item Any free agent may re-sign with their team up until the draft. Any player who is a Free Agent during the draft may
	 not re-sign with their old team until the end of session 1. If a team did not benefit during the draft from said player being
	 off their roster then that player may re-sign with their old team under normal free agency rules. 
\end{deepEnumerate}